
##  Airplane inspection with Rotors simulator

In order to execute the mission defined in configs/mission/behavior_tree_mission_file.yaml, perform the following steps:

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

![gazebo_launched.png](https://i.ibb.co/CzWjYft/Selection-003.png)

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh
![main_launcher.png](https://i.ibb.co/SVwXrXH/Selection-004.png)

Here there is a video that shows how to launch the project:

[ ![Launch project](https://img.youtube.com/vi/nhpBA1yxNwo/0.jpg)](https://www.youtube.com/watch?v=nhpBA1yxNwo)

Here there is a video that shows the correct execution of the mission:   

[ ![Rotors Simulator Mission](https://img.youtube.com/vi/xNEpnWfEzSo/0.jpg)](https://www.youtube.com/watch?v=xNEpnWfEzSo)

