#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1

export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/airplane_inspection_rotors_simulator

. ${AEROSTACK_STACK}/setup.sh
OPEN_ROSCORE=1

gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# alphanumeric_viewer                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "alphanumeric_viewer"  --command "bash -c \"
roslaunch alphanumeric_viewer alphanumeric_viewer.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &
